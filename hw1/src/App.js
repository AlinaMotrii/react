import React, { useState } from "react";

import "./App.css";
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";
import styles from "./Components/Modal/Modal.module.scss"
// import styles from "./Components/Button/Button.module.scss"

function App() {
  const [showModal1, setShowModal1] = useState(false);
  const [showModal2, setShowModal2] = useState(false);

  function openFirstModal() {
    setShowModal1(true);
    setShowModal2(false);
  }

  function openSecondModal() {
    setShowModal1(false);
    setShowModal2(true);
  }

  //   function closeModal() {
  //     setShowModal1(false);
  //     setShowModal2(false);
  //  }
  //   const clickButton = (click )=> {
  //    {closeModal}
  //  }
  return (
    <div className="App">
      <Button
        text="Open first modal"
        backgroundColor="cadetblue"
        onClick={openFirstModal}
      />
      <Button
        text="Open second modal"
        backgroundColor="grey"
        onClick={openSecondModal}
      />

      {showModal1 && (
        <Modal
          header="STEINWAY & SONS"
          text="STEINWAY grand pianos are the gold standard of musical instruments, representing 170 years of dedication to craftsmanship and uncompromised expression. This dedication has led over 19 out of 20 concert pianists to choose STEINWAY grand pianos — and it is why the instruments remain at the heart of cultured homes the world over."
          onClick={() => setShowModal1(false)}
          closeButton={true}
          actions={
            <>
              <button className={styles.firstModalBtn}>OK</button>
              <button className={styles.firstModalBtn}>CANCEL</button>
            </>
          }
        />
      )}
      {showModal2 && (
        <Modal
          header="C.Bechstein Concert"
          text="Fabulous masterpieces for the highest demands, the ideal sound of many pianists worldwide. The perfect action renders every nuance. Unsurpassed excellence."
          onClick={() => setShowModal2(false)}
          closeButton={false}
          actions={
            <>
              <button className={styles.secondModalBtn}>MORE</button>
              <button className={styles.secondModalBtn}>SKIP</button>
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
