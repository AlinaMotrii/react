import React from "react";
import styles from "./Modal.module.scss";
const Modal = (props) => {
  return (
    <>
      <div className={styles.modal}>
        <div className={styles.headerWrap}>
          <h2 className={styles.title}>{props.header}</h2>
        </div>
        <p className={styles.text}>{props.text}</p>
        {props.closeButton && (
          <button onClick={props.onClick} className={styles.buttonClose}>
            &times;
          </button>
        )}
        {props.actions}
      </div>
      <div className={styles.overlay} onClick={props.onClick}></div>
    </>
  );
};

export default Modal;
