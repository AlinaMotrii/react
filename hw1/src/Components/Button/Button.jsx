import React from "react";
import styles from "./Button.module.scss"
const Button = (props) => {
  return <button className={styles.btn } style={{ backgroundColor: props.backgroundColor }} onClick={props.onClick}>{props.text}</button>
};

export default Button;
